using UnityEngine;
using System.Collections;


public class BallWhack : MonoBehaviour {
    protected GameObject mBall;
    protected Camera mCamera;

    AnimalMouseManager mLeftMouse = new AnimalMouseManager();
	// Use this for initialization
	void Start () 
    {
        mBall = GameObject.FindGameObjectWithTag("BALL");
        mCamera = GameObject.FindGameObjectWithTag("CAMERA").GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        //mBall.rigidbody.AddForceAtPosition(new Vector3(-10, -10, -10), mBall.transform.position);
        if (Input.GetMouseButtonUp(0))
        {
            //Debug.Log("adding force");
            Vector3 mouse = Input.mousePosition;
            mouse.x /= (float)Screen.width;
            mouse.y /= (float)Screen.height;
            Ray toCast = mCamera.ViewportPointToRay(mouse);
            RaycastHit hit;
            int layerMask = 1 << 8;
            if (Physics.Raycast(toCast, out hit, Mathf.Infinity, layerMask))
            {
                GameObject.FindGameObjectWithTag("TEMP").transform.position = hit.point;
                mBall.rigidbody.AddForceAtPosition((mCamera.transform.position - mBall.transform.position).normalized * -100, hit.point, ForceMode.Impulse);
            }
            //mBall.rigidbody.AddExplosionForce(1000, hit.point, 5,-0.5f);
        }



        //TODO should make an elastic followy type script on the ball that also interacts with all the hit collision nonsense
        //lol do it
        //later...


        Vector3 oldPos;
        if (Input.GetMouseButtonDown(1))
            mLeftMouse.mousePressed(Input.mousePosition);
        if (Input.GetMouseButtonUp(1))
            mLeftMouse.mouseReleased(Input.mousePosition);
        else if (mLeftMouse.isCursorDown)
        {
            Vector3 pos = mLeftMouse.getMouseChange();
            mLeftMouse.mouseMoved(Input.mousePosition);
            Vector3 npos = mLeftMouse.getMouseChange();
            Vector3 dp = npos - pos;
            Debug.Log(dp);
            //old position
            oldPos = mCamera.transform.position;
            mCamera.transform.LookAt(mBall.transform);
            mCamera.transform.position = AnimalMath.rotateAbout(mCamera.transform.position, mCamera.transform.up, mBall.transform.position, -dp.y/500.0f, dp.x/500.0f);
            //check if old position is valid
            //i.e. do some raycasting here
            //TODO
            mCamera.transform.LookAt(mBall.transform);
        }

        oldPos = mCamera.transform.position;
        float zoom = Input.GetAxis("Mouse ScrollWheel");
        Debug.Log(zoom);
        mCamera.transform.position += mCamera.transform.forward * zoom * 100;
        //TODO camera hit collision nonsense here
	}

}
