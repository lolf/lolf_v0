using UnityEngine;
using System.Collections;

public class AnimalMath
{
    static public Vector3 rotateAbout(Vector3 aToRotate, Vector3 aUp, Vector3 aAbout, float theta, float phi)
    {
        Vector3 toRot = aToRotate - aAbout;
        aUp.Normalize();
        Vector3 aSide = Vector3.Cross(aUp, toRot).normalized;
        Vector3 composite = theta * aUp + phi * aSide;
        composite.Normalize();
        float rad = Mathf.Sqrt(theta*theta + phi*phi);
        toRot = Vector3.RotateTowards(toRot, composite, rad, 0);
        return toRot + aAbout;
    }
}
